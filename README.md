# Dwebly Debugger
Dwebly debugger is a debugger and logging tools for Dwebly's microservice applications.

### Requirements
Dwebly debugger requires `google/cloud-logging ^1.24` to run.

### Installation

Install package
```sh
composer require dwebly/debugger
```

##### To enable logging to Google Cloud Logging:

Register Middleware in `app\Http\Kernel.php`
```sh
protected $middleware = [
    ...
    \Dwebly\Debugger\Middleware\LoggingRequest::class,
    ...
];
```

## License

proprietary
