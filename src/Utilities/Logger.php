<?php

namespace Dwebly\Debugger\Utilities;

use Dwebly\Debugger\Events\Loggings\LoggingEvent;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

/**
 * Logger Utilities
 * @author  Danial <danial@dwebly.com>
 *
 */
class Logger
{
    /**
     * Constant
     */

    /**
     * vars
     */
    public $severity;
    public $msg;
    public $debug;

    /**
     * Create a new logger instance.
     *
     * @param string $severity
     * @return void
     */
    public function __construct($severity)
    {
        $this->severity = $severity;
    }


    /**
     * Create a new logger instance.
     *
     * @param string $severity
     * @return void
     */
    public function dispatch(){
        // Dispatch Logging Event
        LoggingEvent::dispatch($this);
    }

    /**
     * Store log information
     * @param array $request request data
     * @return void
     */
    public function store($request = []): void
    {
        $request = (!empty($request)) ? $request : self::requestInfo();
        $header = $request['HEADER'];

        // IP Address
        $directIp = (isset($header['ip-address'])) ? $header['ip-address'] : null;
        $ipAddress = (isset($header['x-forwarded-for'][0]))
                        ? $header['x-forwarded-for'][0]
                        : $directIp;
        // Hosts
        $host = config('app.name');

        // Origin or referrer
        $referrer = (isset($header['referer'][0])) ? $header['referer'][0] : null;
        $origin =   (isset($header['origin'][0]))
                        ? $header['origin'][0]
                        : $referrer;

        $log = $this->msg;
        if(isset($origin)){
            $originParsed = parse_url($origin);
            if(isset($originParsed['host'])){
                $log .= " ----- (" . $originParsed['host'] . ")";
            }
        }

        $debugLog = [
            'debug' => $this->debug,
            'ip-address' => $ipAddress,
            'host' => $host,
            'origin' => $origin,
            '_SEVERITY' => $this->severity,
            '_ENVIRONMENT' => App::environment(),
            '_SCOPE' => 'API',
            '_SERVICE' => $host,
            '_REQUEST' => $request
        ];

        switch ($this->severity) {
            case 'critical':
                if(!App::environment('local')){
                    Log::critical($log, $debugLog);
                    Log::channel('gcp')->critical($log, $debugLog);
                }
                break;
            case 'error':
                if(!App::environment('local')){
                    Log::error($log, $debugLog);
                    Log::channel('gcp')->error($log, $debugLog);
                }
                break;
            case 'warning':
                if(!App::environment('local')){
                    Log::warning($log, $debugLog);
                    Log::channel('gcp')->warning($log, $debugLog);
                }
                break;
            default:
                if(!App::environment('local')){
                    Log::info($log, $debugLog);
                    Log::channel('gcp')->info($log, $debugLog);
                }
                break;
        }
    }

    /**
     * Retireve request information
     */
    public function requestInfo($encoded = true){

        $header = request()->header();
        $header['ip-address'] = request()->ip();

        $server = json_decode(json_encode(request()->server()), true);
        $header = json_decode(json_encode(request()->header()), true);
        $body = json_decode(json_encode(request()->all()), true);

        return [
            'EVENT' => false,
            'URL' => request()->getUri(),
            'METHOD' => request()->getMethod(),
            'HEADER' => $header,
            'SERVER' => $server,
            'BODY' => $body,
            // 'RESPONSE' => $response->getContent()
        ];
    }
}
