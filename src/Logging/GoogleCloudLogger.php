<?php

namespace Dwebly\Debugger\Logging;

use Google\Cloud\Logging\LoggingClient;
use Illuminate\Support\Facades\App;

class GoogleCloudLogger
{
    /**
     * Create a custom Monolog instance.
     *
     * @param  array  $config
     * @return \Monolog\Logger
     */
    public function __invoke(array $config)
    {
        $logging = new LoggingClient([
           'keyFile' => json_decode(file_get_contents(__DIR__ . '/../_keys/' . App::environment().'-google-logging-file.json'), true)
        ]);

        return $logging->psrLogger('app');
    }
}
