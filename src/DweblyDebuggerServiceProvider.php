<?php

namespace Dwebly\Debugger;

use Dwebly\Debugger\Exceptions\CustomHandler;
use Dwebly\Debugger\Logging\GoogleCloudLogger;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Support\ServiceProvider;

class DweblyDebuggerServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Set config for gcp logging
        $this->app['config']->set('logging.channels.gcp', [
                'driver' => 'custom',
                'via' => GoogleCloudLogger::class,
            ]
        );

        // Register custom EventServiceProvider
        $this->app->register(EventServiceProvider::class);

        // Register Custom Handler
        /** @var ExceptionHandler $exceptionHandler */
        $exceptionHandler = resolve(ExceptionHandler::class);
        (new CustomHandler($exceptionHandler))->register();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
