<?php

namespace Dwebly\Debugger\Listeners\LoggingEvent;

use Dwebly\Debugger\Events\Loggings\LoggingEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Session;

class StoreLogsListener implements ShouldQueue
{

    /*
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoggingEvent  $event
     * @return void
     */
    public function handle(LoggingEvent $event)
    {
        if(isset($event->logger)){
            $requestInfo = (isset($event->requestInfo)) ? $event->requestInfo : [];
            $event->logger->store($requestInfo);
        }
    }
}
