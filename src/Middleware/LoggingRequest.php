<?php
namespace Dwebly\Debugger\Middleware;

use Closure;
use Dwebly\Debugger\Utilities\Logger;

class LoggingRequest {

    public function handle($request, Closure $next)
    {
        $excludes = ['healthcheck', '', '/'];
        if(!in_array(request()->path(), $excludes)){
            $logger = new Logger('info');
            $logger->msg =  "[".request()->getMethod()."] /".request()->path();
            $logger->dispatch();
        }
        return $next($request);
    }
}
?>
