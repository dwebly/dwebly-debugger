<?php

namespace Dwebly\Debugger;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Dwebly\Debugger\Events\Loggings\LoggingEvent;
use Dwebly\Debugger\Listeners\LoggingEvent\StoreLogsListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        LoggingEvent::class => [
            StoreLogsListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
