<?php

namespace Dwebly\Debugger\Exceptions;

use BadMethodCallException;
use Dwebly\Debugger\Utilities\Logger;
use ErrorException;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Client\ConnectionException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\ValidationException;
use MeiliSearch\Exceptions\ApiException;
use PDOException;
use Throwable;

class CustomHandler
{
    public $exceptionHandler;

    public function __construct($exceptionHandler)
    {
        $this->exceptionHandler = $exceptionHandler;
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->exceptionHandler->reportable(function (Throwable $exception) {
            // if(App::environment('production')){
            //     abort(500);
            // }
            //
            // if($exception instanceof \Laravel\Passport\Exceptions\OAuthServerException){
            //     if(App::environment('production')){
            //         abort(500);
            //     }
            // }
        });

        $this->exceptionHandler->renderable(function (Exception $exception) {

            // Default error handler
            $logSeverity = 'critical';
            $debug = [
                'message' => 'This exception has no handler',
                'type' => get_class($exception)
            ];
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            if (config('app.debug') === false) {
                // To prevent php explosive error if not in debugging mode
                $error = $exception->getMessage() ?? $this->_getDefaultMessage(500);
            }

            // AuthenticationException
            if ($exception instanceof AuthenticationException) {
                if($exception->redirectTo() == null){
                    $error = $exception->getMessage();
                    $debug = [
                        'code' => 401,
                        'message' => $exception->getMessage(),
                        'type' => 'AuthenticationException'
                    ];
                    $code = 401;
                }else{
                    $error = null;
                }
                $logSeverity = 'warning';
            }

            // ValidationException
            if ($exception instanceof ValidationException) {
                $error = $exception->getMessage() ?? $this->_getDefaultMessage($exception->status);
                $debug = [
                    'code' => $exception->status,
                    'message' =>$exception->getMessage(),
                    'type' => 'ValidationException'
                ];
                $code = $exception->status;
                $logSeverity = 'error';
            }

            // HttpException
            if ($exception instanceof HttpException) {
                $error = $exception->getMessage() ?? $this->_getDefaultMessage($exception->getStatusCode());
                $debug = [
                    'code' => $exception->getStatusCode(),
                    'message' => $exception->getMessage() ?? $this->_getDefaultMessage($exception->getStatusCode()),
                    'headers' => $exception->getHeaders(),
                    'type' => 'HttpException'
                ];
                $code = $exception->getStatusCode();
                if($code === 404){
                    $logSeverity = 'warning';
                }else{
                    $logSeverity = 'error';
                }
            }

            // BadMethodCalLException
            if ($exception instanceof BadMethodCallException) {
                $error = $this->_getDefaultMessage(500);
                $debug = [
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                    'type' => 'BadMethodCallException'
                ];
                $logSeverity = 'warning';
            }

            // ErrorException
            if ($exception instanceof ErrorException) {
                $error = $this->_getDefaultMessage(500);
                $debug = [
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                    'type' => 'ErrorException'
                ];
                $logSeverity = 'critical';
            }

            // ConnectionException
            if ($exception instanceof ConnectionException) {
                $error = $this->_getDefaultMessage(500);
                $debug = [
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                    'type' => 'ConnectionException'
                ];
                $logSeverity = 'critical';
            }

            // PDOException
            if ($exception instanceof PDOException) {
                $error = $this->_getDefaultMessage(500);
                $debug = [
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                    'type' => 'PDOException'
                ];
                $logSeverity = 'critical';
            }

            // QueryException
            if ($exception instanceof QueryException) {
                $error = $this->_getDefaultMessage(500);
                $debug = [
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                    'sql' => $exception->getSql(),
                    'err_info' => $exception->errorInfo,
                    'type' => 'QueryException'
                ];
                $logSeverity = 'critical';
            }

            // ApiException
            if ($exception instanceof \MeiliSearch\Exceptions\ApiException) {
                $error = $exception->message;
                $debug = [
                    'code' => $exception->errorCode,
                    'message' => $exception->message,
                    'errorLink' => $exception->errorLink,
                    'httpBody' => $exception->httpBody,
                    'errorType' => $exception->errorType,
                    'type' => 'MeiliSearch\Exceptions\ApiException'
                ];
                $logSeverity = 'error';
            }

            // Unreachable Exception
            $unreachableException = ['RedisException'];
            if(in_array(get_class($exception), $unreachableException)){
                $error = $exception->getMessage();
                $debug = [
                    'message' => 'This exception is part of known but unreachable exception',
                    'type' => get_class($exception)
                ];
                $logSeverity = 'error';
            }

            // Log first
            $logger = new Logger($logSeverity);
            $logger->msg = (isset($error)) ? $error : $exception->getMessage();
            $logger->debug = $debug;
            $logger->dispatch();

            $debugLevel = (is_numeric(request()->input('debug')) ? request()->input('debug') : null)
                ?? ((config('app.debug') === true) ? 1 : 0);

            if (isset($error) && $debugLevel <= 1) {
                $respond['error'] = $error;
                //Display debug detail in debug mode
                if (config('app.debug') === true) {
                    $respond['debug'] = $debug;
                }

                if (request()->wantsJson()) {
                    return response()->json($respond, $code);
                }
            }

        });
    }

    /**
     * Get default error message by status code
     *
     * @return void
     */
    private function _getDefaultMessage($code)
    {
        $defaultMessage = [
            100 => 'Continue',
            101 => 'Switching Protocols',
            102 => 'Processing', // WebDAV; RFC 2518
            103 => 'Early Hints', // RFC 8297
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information', // since HTTP/1.1
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content', // RFC 7233
            207 => 'Multi-Status', // WebDAV; RFC 4918
            208 => 'Already Reported', // WebDAV; RFC 5842
            226 => 'IM Used', // RFC 3229
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found', // Previously "Moved temporarily"
            303 => 'See Other', // since HTTP/1.1
            304 => 'Not Modified', // RFC 7232
            305 => 'Use Proxy', // since HTTP/1.1
            306 => 'Switch Proxy',
            307 => 'Temporary Redirect', // since HTTP/1.1
            308 => 'Permanent Redirect', // RFC 7538
            400 => 'Bad Request',
            401 => 'Unauthorized', // RFC 7235
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required', // RFC 7235
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed', // RFC 7232
            413 => 'Payload Too Large', // RFC 7231
            414 => 'URI Too Long', // RFC 7231
            415 => 'Unsupported Media Type', // RFC 7231
            416 => 'Range Not Satisfiable', // RFC 7233
            417 => 'Expectation Failed',
            418 => 'I\'m a teapot', // RFC 2324, RFC 7168
            421 => 'Misdirected Request', // RFC 7540
            422 => 'Unprocessable Entity', // WebDAV; RFC 4918
            423 => 'Locked', // WebDAV; RFC 4918
            424 => 'Failed Dependency', // WebDAV; RFC 4918
            425 => 'Too Early', // RFC 8470
            426 => 'Upgrade Required',
            428 => 'Precondition Required', // RFC 6585
            429 => 'Too Many Requests', // RFC 6585
            431 => 'Request Header Fields Too Large', // RFC 6585
            451 => 'Unavailable For Legal Reasons', // RFC 7725
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            506 => 'Variant Also Negotiates', // RFC 2295
            507 => 'Insufficient Storage', // WebDAV; RFC 4918
            508 => 'Loop Detected', // WebDAV; RFC 5842
            510 => 'Not Extended', // RFC 2774
            511 => 'Network Authentication Required', // RFC 6585
        ];
        return (isset($defaultMessage[$code])) ? $defaultMessage[$code] : $defaultMessage[500];
    }
}
