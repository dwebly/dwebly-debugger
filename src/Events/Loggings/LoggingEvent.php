<?php

namespace Dwebly\Debugger\Events\Loggings;

use Dwebly\Debugger\Utilities\Logger;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Carbon\Carbon;

class LoggingEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var \Dwebly\Debugger\Utilities\Logger
     */
    public $logger;
    public $requestInfo;

    /**
     * Create a new event instance.
     *
     * @param \Dwebly\Debugger\Utilities\Logger $logger
     * @return void
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
        $this->requestInfo = $logger->requestInfo();
        $this->requestInfo['actual_datetime'] = Carbon::now();
        $this->requestInfo['EVENT'] = true;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        // return new PrivateChannel('channel-name');
    }
}
